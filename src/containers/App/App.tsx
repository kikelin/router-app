import React from 'react';
import Header from '../../components/Header/Header';
import Menu from '../../components/Menu/Menu';
import Home from '../../containers/Home/Home';
import './App.css';
import { BrowserRouter } from 'react-router-dom';
import Routes from '../../components/Routes/Routes';

function App() {
  return (
    <div>
      <BrowserRouter>
        <Header/>
        <Menu/>
        <Routes/>
      </BrowserRouter>
    </div>
  );
}

export default App;
