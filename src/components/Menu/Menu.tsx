import React, { SFC } from 'react';
import { Link } from 'react-router-dom';

const Header: SFC = () => {
    return (
        <div>
            <nav className="navbar navbar-expand-sm bg-light">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link to='/' className="nav-link">Home</Link>
                    </li>
                    <li className="nav-item">
                        <Link to='/cart' className="nav-link">Cart</Link>
                    </li>
                    <li className="nav-item">
                        <Link to='/contactus' className="nav-link">Contact Us</Link>
                    </li>
                </ul>
            </nav>        
        </div>
    )
}

export default Header;





