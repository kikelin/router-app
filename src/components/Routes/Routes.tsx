import React, {SFC} from 'react';
import {Switch, Route} from 'react-router-dom'
import Home from '../../containers/Home/Home';
import Cart from '../../containers/Cart/Cart';
import ContactUs from '../../containers/ContactUs/ContactUs';

const Routes: SFC = () => {
    return (
        <Switch>
            <Route path="/" component={Home} exact/>
            <Route path="/cart" component={Cart}/>
            <Route path="/contactus" component={ContactUs}/>
        </Switch>
    );
}

export default Routes;